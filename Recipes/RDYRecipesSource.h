//
//  RDYRecipesSource.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 12/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDYRecipesListDataSource.h"

@interface RDYRecipesSource : NSObject <RDYRecipesListDataSource>

@end
