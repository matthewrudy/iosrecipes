//
//  main.m
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RDYAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RDYAppDelegate class]));
    }
}
