//
//  RDYViewController.m
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RDYViewController.h"

@interface RDYViewController ()

@end

@implementation RDYViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = self.recipe.title;

    self.directionsView.text = self.recipe.directions;
    self.prepTimeView.text = [NSString stringWithFormat: @"%d %@", self.recipe.prepTime, NSLocalizedString(@"minutes", nil)];
    self.imageView.image = self.recipe.image;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
