//
//  RDYRecipe.m
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RDYRecipe.h"

@implementation RDYRecipe

+(RDYRecipe *)recipeWithTitle:(NSString *)title
                   directions:(NSString *)directions
                     prepTime:(NSInteger)prepTime
                        image:(UIImage *)image {
    
    RDYRecipe *recipe = [self new];
    recipe.title = title;
    recipe.directions = directions;
    recipe.prepTime = prepTime;
    recipe.image = image;
    
    return recipe;
}

@end
