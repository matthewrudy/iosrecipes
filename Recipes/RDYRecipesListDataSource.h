//
//  RDYRecipesListDataSource.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 12/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDYRecipe.h"

@protocol RDYRecipesListDataSource <NSObject>

-(NSInteger) recipeCount;
-(RDYRecipe *) recipeAtIndex:(NSUInteger)index;
-(void) deleteRecipeAtIndex:(NSUInteger)index;
-(RDYRecipe *) createNewRecipe;

@end
