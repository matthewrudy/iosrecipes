//
//  RDYViewController.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDYRecipe.h"

@interface RDYViewController : UIViewController

@property(nonatomic, strong) RDYRecipe *recipe;

@property(nonatomic, strong) IBOutlet UITextView *directionsView;
@property(nonatomic, strong) IBOutlet UILabel *prepTimeView;
@property(nonatomic, strong) IBOutlet UIImageView *imageView;

@end
