//
//  RDYRecipesSource.m
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 12/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RDYRecipesSource.h"
#import "RDYRecipe.h"

@interface RDYRecipesSource ()

@property (nonatomic, strong) NSMutableArray *recipes;

@end

@implementation RDYRecipesSource

-(id)init {
    self = [super init];
    
    if (self) {
        self.recipes = [NSMutableArray arrayWithObjects:
            [RDYRecipe recipeWithTitle:@"Cookies"
                            directions:@"Mix stuff up, then shove it all in the oven"
                              prepTime:3
                                 image:[UIImage imageNamed:@"sammi.jpg"]],
                
            [RDYRecipe recipeWithTitle:@"Tulips"
                            directions:@"Add water, and hope for the best"
                              prepTime:4 
                                 image:[UIImage imageNamed:@"tulip.jpg"]],
                        nil
        ];

    }
    
    return self;
}

-(NSInteger)recipeCount {
    return [self.recipes count];
}

-(RDYRecipe *)recipeAtIndex:(NSUInteger)index {
    return [self.recipes objectAtIndex:index];
}

-(void)deleteRecipeAtIndex:(NSUInteger)index {
    [self.recipes removeObjectAtIndex:index];
}

-(RDYRecipe *)createNewRecipe {
    RDYRecipe *recipe = [RDYRecipe new];
    [self.recipes addObject:recipe];
    return recipe;
}

@end
