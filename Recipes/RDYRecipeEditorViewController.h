//
//  RDYRecipeEditorViewController.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 13/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDYRecipe.h"

@interface RDYRecipeEditorViewController : UIViewController

@property(nonatomic, strong) RDYRecipe *recipe;

@property(nonatomic, strong) IBOutlet UITextField *titleInput;
@property(nonatomic, strong) IBOutlet UITextView *directionsView;
@property(nonatomic, strong) IBOutlet UILabel *prepTimeView;
@property(nonatomic, strong) IBOutlet UIStepper *prepTimeStepper;
@property(nonatomic, strong) IBOutlet UIImageView *imageView;

- (IBAction)changePrepTime:(UIStepper *)sender;

@end
