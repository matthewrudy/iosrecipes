//
//  RDYRecipe.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDYRecipe : NSObject

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *directions;
@property(nonatomic) NSInteger prepTime;
@property(nonatomic, strong) UIImage *image;

+(RDYRecipe *) recipeWithTitle:(NSString *)title
                    directions:(NSString *)directions
                      prepTime:(NSInteger)prepTime
                         image:(UIImage *)image;

@end
