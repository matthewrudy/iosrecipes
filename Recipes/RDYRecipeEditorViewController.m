//
//  RDYRecipeEditorViewController.m
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 13/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RDYRecipeEditorViewController.h"

@interface RDYRecipeEditorViewController ()

@end

@implementation RDYRecipeEditorViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.titleInput.text = self.recipe.title;
    self.directionsView.text = self.recipe.directions;
    self.prepTimeView.text = [NSString stringWithFormat: @"%d %@", self.recipe.prepTime, NSLocalizedString(@"minutes", nil)];
    if (nil != self.recipe.image) {
        self.imageView.image = self.recipe.image;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePrepTime:(UIStepper *)sender {
    self.recipe.prepTime = (NSInteger) [sender value];
    self.prepTimeView.text = [NSString stringWithFormat: @"%d %@", self.recipe.prepTime, NSLocalizedString(@"minutes", nil)];
}

@end
