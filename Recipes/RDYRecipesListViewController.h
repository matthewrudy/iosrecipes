//
//  RDYRecipesListViewController.h
//  Recipes
//
//  Created by Matthew Rudy Jacobs on 12/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RDYRecipesListDataSource.h"

@interface RDYRecipesListViewController : UITableViewController

@property (nonatomic, strong) id <RDYRecipesListDataSource> recipeSource;

@end
