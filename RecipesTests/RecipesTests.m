//
//  RecipesTests.m
//  RecipesTests
//
//  Created by Matthew Rudy Jacobs on 08/11/2012.
//  Copyright (c) 2012 Matthew Rudy Jacobs. All rights reserved.
//

#import "RecipesTests.h"
#import "RDYRecipe.h"

@implementation RecipesTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testInitializer {
    RDYRecipe *recipe = [RDYRecipe recipeWithTitle:@"A title"
                                        directions:@"do something"
                                          prepTime:2
                                             image:[UIImage imageNamed: @"tulip.jpg"]
                         ];
    STAssertEqualObjects(@"A title",recipe.title, @"the recipe title");
    STAssertEqualObjects(@"do something", recipe.directions, @"the recipe directions");
    STAssertEquals(2, recipe.prepTime, @"the recipe prep time");
    STAssertEqualObjects([UIImage imageNamed: @"tulip.jpg"], recipe.image, @"the recipe image");
    
}

@end
